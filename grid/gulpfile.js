const autoprefixer = require("gulp-autoprefixer");
const sass = require("gulp-sass");
const gulp = require("gulp");
const clean = require("gulp-clean");

gulp.task("HTML", () => {
    return gulp.src("src/*.html").pipe(gulp.dest("build"));
});

gulp.task("CSS", () => {
    return gulp
        .src("src/scss/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(gulp.dest("build"));
});

gulp.task("JS", () => {
    return gulp.src("src/js/**/*.js").pipe(gulp.dest("build"));
});

gulp.task("clean", () => {
    return gulp.src("build", { read: false }).pipe(clean());
});

gulp.task("build", () => {
    gulp.series("clean", gulp.parallel("CSS", "HTML", "JS"));
});
