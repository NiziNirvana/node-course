/**Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

Задание

Реализовать класс Employee, в котором будут следующие свойства
 - name (имя), age (возраст), salary (зарплата).
 Сделайте так, чтобы эти свойства заполнялись при создании объекта.
    Создайте геттеры и сеттеры для этих свойств.
    Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
Для класса Programmer перезапишите геттер для свойства salary.
 Пусть он возвращает свойство salary, умноженное на 3.
Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.*/

class Employee {
    constructor(name, age, salary) {
        this.age = age;
        this.name = name;
        this.salary = salary;
    }

    get getAge() {
        return  this.age;
    }
    set setAge(age) {
        this.age = age;
    }
    get getName() {
        return  this.name;
    }
    set setName(name) {
        this.name = name;
    }
    get getSalary() {
        return  this.salary;
    }
    set setSalary(sal) {
        this.salary = sal;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get getSalary() {
        return  this.salary*3;
    }
}


let p1 = new Programmer('Andrey', 22, 56);
let p2 = new Programmer('Nizi', 18, 52);
console.log(p1);
console.log(p1.getSalary);
p1.setSalary = 14;
console.log(p1.getSalary);