// Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

/// method 1
//const employeeFull = {...employee, age:41, salary:200}

/// method 2
//const  employeeFull = Object.assign({}, employee, {age:41, salary:200});

/// method 3
const mergeObjs = (...objs) => Object.assign({}, ...objs);
const employeeFull = mergeObjs(employee, {age:41, salary:200});

console.log(employeeFull)