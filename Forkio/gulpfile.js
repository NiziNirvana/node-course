const gulp = require("gulp");
const {src, dest, series, parallel} = gulp
const sass = require("gulp-sass")(require('sass'));
// const minify = require('gulp-minify');
// const uglify = require('gulp-js-uglify');
const cleanCSS = require('gulp-clean-css');
const concat  = require ('gulp-concat') ;
const imagemin = require ('gulp-imagemin') ;
const  autoprefixer  =  require ('gulp-autoprefixer');
const clean = require('gulp-clean')


function style() {
    return src(['src/scss/**.scss'])
        .pipe(concat('styles.min.css'))
        .pipe(sass({outputStyle:'expanded'}).on('error',sass.logError))
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(dest("dist/css/"))
}

function images() {
    return src(['src/img/**/**.png'])
        .pipe(imagemin())
        .pipe(dest("dist/img/"))
}

function clear() {
    return src('./dist', {read: false})
        .pipe(clean());
}

function scripts() {

}



// gulp.task('watch', function (){
//     gulp.watch(['src/scss/**'],['sass']);
// });
// gulp.task('default', ['watch']);


exports.default = series(clear, images, style)
exports.style = style;
exports.images = images;



///series and parallels
// drop down? button-menu