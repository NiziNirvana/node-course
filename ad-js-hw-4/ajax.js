// let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
//
// function jsonParse(url) {
//     let request = new XMLHttpRequest();
//     request.open('GET', url, false);
//     request.send();
//     return JSON.parse(request.responseText);
// }
//
// class SWMovie {
//     constructor(episodeId, name, openingCrawl) {
//         this.ep = episodeId;
//         this.title = name;
//         this.desc = openingCrawl;
//     }
//     get info() {
//         return `<li>Episode ${this.ep}: ${this.title}</li><li>${this.desc}</li>`
//     }
// }
//
// class SWMovieFull extends SWMovie {
//     constructor(ep, title, desc, characters) {
//         super(ep, title, desc);
//         this.starring = characters;
//     }
//
//     get characters() {
//         return this.starring.map(function(url) {
//             const ch = jsonParse(url);
//             return  new SWPerson(ch.name, ch.gender, ch.height, ch.mass, ch.eyeColor, ch.hairColor, ch.skinColor, ch.specie);
//         });
//     }
// }
//
// class SWSpec {
//     constructor(name, classification, language) {
//         this.name = name;
//         this.class = classification;
//         this.lang = language;
//     }
// }
//
// class SWPerson {
//     constructor(name, gender, height, mass, eyeColor, hairColor, skinColor, specie) {
//         this.name = name;
//         this.sex = gender;
//         this.height = `${height} cm`;
//         this.weight = `${mass} kg`;
//         this.eye_color = eyeColor;
//         this.hair_color = hairColor;
//         this.skin_color = skinColor;
//         if (specie) {
//             const obj = jsonParse(specie);
//             this.specie = new SWSpec(obj.name, obj.classification, obj.language)
//         } else {
//             this.specie = new SWSpec("Human", "Homo Sapiens", "English");
//         }
//     }
//     get info() {
//         return `<li>Name: ${this.name}</li>\
//         <li>Gender: ${this.sex}</li>\
//         <li>Height: ${this.height}</li>\
//         <li>Weight: ${this.weight}</li>\
//         <li>Eye Color: ${this.eyeColor}</li>\
//         <li>Hair Color: ${this.hairColor}</li>\
//         <li>Skin Color: ${this.skinColor}</li>\
//         <ul>Specie:<li>Name: ${this.specie.name}</li>\
//         <li>Class: ${this.specie.class}</li>\
//         <li>Language: ${this.specie.lang}</li></ul>`
//     }
// }
//
// let req = new XMLHttpRequest();
// req.open('GET', 'https://ajax.test-danit.com/api/swapi/films', false);
// req.send();
//
// let moviesList = JSON.parse(req.responseText).map(function(item) {
//     return new SWMovie(item.episodeId, item.name, item.openingCrawl, item.characters);
// })
//
// let moviesListFull = JSON.parse(req.responseText).map(function(item) {
//     return new SWMovieFull(item.episodeId, item.name, item.openingCrawl, item.characters);
// })
//
// // ВСЕ ФИЛЬМЫ
//
// moviesListFull.forEach(function(movie, index) {
//     console.log(`ФИЛЬМ movie-${index}\n\n`);
//     console.log(`<ul id=movie-${index}>${movie.info}</ul>`);
//     console.log("\n\n");
// });
//
// // ВСЕ ПЕРСОНАЖИ
//
// moviesListFull.forEach(function(movie, index) {
//     console.log(`ПЕРСОНАЖИ ФИЛЬМА movie-${index}\n\n`);
//     movie.characters.forEach(function(character, ind) {
//         console.log(`<ul id=movie-${index}_person-${ind}>${character.info}</ul>`);
//         console.log("\n");
//     })
//     console.log("\n\n");
// });