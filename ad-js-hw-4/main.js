/**Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

    Технические требования:

    Отправить AJAX запрос по адресу https://ajax.test-danit.com/api/swapi/films
 и получить список всех фильмов серии Звездные войны

    Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей
 можно получить из свойства characters.
    Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо
 указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран
 под названием фильма.


    Необязательное задание продвинутой сложности

Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую.
 Желательно найти вариант на чистом CSS без использования JavaScript.*/

let button = document.querySelector('.btn');
button.addEventListener('click', function() {
    getMovies();
    getCharacters();
})

function jsonParse(url) {
    let request = new XMLHttpRequest();
    request.open('GET', url, false);
    request.send();
    return JSON.parse(request.responseText);
}

class SWMovie {
    constructor(episodeId, name, openingCrawl) {
        this.ep = episodeId;
        this.title = name;
        this.desc = openingCrawl;
    }
    get info() {
        return `<li>Episode ${this.ep}: ${this.title}</li><li>${this.desc}</li>`
    }
}

class SWMovieFull extends SWMovie {
    constructor(ep, title, desc, characters) {
        super(ep, title, desc);
        this.starring = characters;
    }

    get characters() {
        return this.starring.map(function(url) {
            const ch = jsonParse(url);
            return  new SWPerson(ch.name, ch.gender, ch.height, ch.mass, ch.eyeColor, ch.hairColor, ch.skinColor, ch.specie);
        });
    }
}

class SWSpec {
    constructor(name, classification, language) {
        this.name = name;
        this.class = classification;
        this.lang = language;
    }
}

class SWPerson {
    constructor(name, gender, height, mass, eyeColor, hairColor, skinColor, specie) {
        this.name = name;
        this.sex = gender;
        this.height = `${height} cm`;
        this.weight = `${mass} kg`;
        this.eye_color = eyeColor;
        this.hair_color = hairColor;
        this.skin_color = skinColor;
        if (specie) {
            const obj = jsonParse(specie);
            this.specie = new SWSpec(obj.name, obj.classification, obj.language)
        } else {
            this.specie = new SWSpec("Human", "Homo Sapiens", "English");
        }
    }
    get info() {
        return `<li>Name: ${this.name}</li>\
        <li>Gender: ${this.sex}</li>\
        <li>Height: ${this.height}</li>\
        <li>Weight: ${this.weight}</li>\
        <li>Eye Color: ${this.eye_color}</li>\
        <li>Hair Color: ${this.hair_color}</li>\
        <li>Skin Color: ${this.skin_color}</li>\
        <ul>Specie:<li>Name: ${this.specie.name}</li>\
        <li>Class: ${this.specie.class}</li>\
        <li>Language: ${this.specie.lang}</li></ul>`
    }
}

let req = new XMLHttpRequest();
req.open('GET', 'https://ajax.test-danit.com/api/swapi/films', false);
req.send();

// let moviesList = JSON.parse(req.responseText).map(function(item) {
//     return new SWMovie(item.episodeId, item.name, item.openingCrawl, item.characters);
// })

let moviesListFull = JSON.parse(req.responseText).map(function(item) {
    return new SWMovieFull(item.episodeId, item.name, item.openingCrawl, item.characters);
})

// ВСЕ ФИЛЬМЫ
function getMovies() {
    moviesListFull.forEach(function (movie, index) {
        //console.log(`ФИЛЬМ movie-${index}\n\n`);
        document.body.innerHTML += `<ul id=movie-${index}>${movie.info}</ul>`;
    });
}

// ВСЕ ПЕРСОНАЖИ
function getCharacters() {
    moviesListFull.forEach(function(movie, index) {
        let p = document.createElement('P');
        p.innerHTML = "Characters in order of appearance:\n"
        document.getElementById(`movie-${index}`).append(p);
        let ul = document.createElement('UL',);
        movie.characters.forEach(function(character, ind) {
            ul.innerHTML += character.info;
        })
        document.getElementById(`movie-${index}`).append(ul);
    });
}