
    /**Выведите этот массив на экран в виде списка
     *  (тег ul - список должен быть сгенерирован с помощью Javascript).
    На странице должен находиться div с id="root",
     куда и нужно будет положить этот список (похожая задача была дана в модуле basic).

    Перед выводом обьекта на странице, нужно проверить его на корректность
     (в объекте должны содержаться все три свойства - author, name, price).
     Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
        Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.*/

    const books = [
        {
            author: "Скотт Бэккер",
            name: "Тьма, что приходит прежде",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Воин-пророк",
        },
        {
            name: "Тысячекратная мысль",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Нечестивый Консульт",
            price: 70
        },
        {
            author: "Дарья Донцова",
            name: "Детектив на диете",
            price: 40
        },
        {
            author: "Дарья Донцова",
            name: "Дед Снегур и Морозочка",
        }
    ];

    class Book {

        constructor(author, name, price) {
            this.author = author;
            this.name = name;
            this.price = price;
        }

        get htmlList() {
            return this.toValidHtmlList();
        }

        get isValid() {
            if (! this.author) {
                console.log(`==> ERROR: Missing field 'author' in ${this}`)
                return false
            }

            if (! this.name) {
                console.log(`==> ERROR: Missing field 'name' in ${this}`)
                return false
            }

            if (! this.price) {
                console.log(`==> ERROR: Missing field 'price' in ${this}`)
                return false
            }

            return true
        }

        toValidHtmlList() {
            if (this.isValid) {
                return `<ul><li>${this.author}</li><li>${this.name}</li><li>${this.price}<li></ul>`;
            }
            return ''
        }
    }

    function refactor(bookItem) {
        return new Book(bookItem.author, bookItem.name, bookItem.price)
    }

    function printHtmlList(booksList) {
        const newBooks = books.map(refactor)
        let result = '<ol>'
        newBooks.forEach(function(item) {
            result += item.htmlList;
        });
        return result + '</ol>'
    }

    console.log(printHtmlList(books));

    document.getElementById('root').innerHTML = printHtmlList(books);

