const gulp = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const sass = require("gulp-sass");
const clean = require("gulp-clean");

gulp.task("clean", () => {
    return gulp.src("dist", { allowEmpty: true, read: false }).pipe(clean());
});

gulp.task("buildCss", () => {
    return gulp
        .src("src/styles/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({ cascade: false }))
        .pipe(gulp.dest("dist"));
});

gulp.task("buildHtml", () => {
    return gulp
        .src("src/index.html", { allowEmpty: true })
        .pipe(gulp.dest("dist"));
});

gulp.task("watch", () => {
    gulp.watch(
        "src/**/*.*",
        gulp.series("clean", gulp.parallel("buildCss", "buildHtml"))
    );
});
