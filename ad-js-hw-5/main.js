/**Написать программу "Я тебя по айпи вычислю"

Технические требования:

    Создать простую HTML страницу с кнопкой Вычислить по IP.
    По нажатию на кнопку - отправить AJAX запрос
по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
    Узнав IP адрес, отправить запрос на сервис
https://ip-api.com/ и получить информацию о физическом адресе.
    Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
    Все запросы на сервер необходимо выполнить с помощью async await.*/

let button = document.querySelector("#btn");

async function getIP() {
    return (fetch("https://api.ipify.org/?format=text")
        .then((response) => response.text()))
}

async function getInfo(ip) {
    return ( fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,regionName,city`)
        .then((response) => response.json()));
}

async function showInfo() {
    const info = await getInfo(await getIP());
    console.log(info);

    if (info.status === "success") {
        document.body.insertAdjacentHTML("afterend", `
      <div class="frame">
          <p>Continent: ${info.continent}</p>
          <p>Country: ${info.country}</p>
          <p>Region: ${info.regionName}</p>
          <p>City: ${info.city}</p>
      </div>`);
    }
}

button.addEventListener("click", showInfo);